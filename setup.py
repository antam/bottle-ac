"""
Flask-AtlassianConnect
-------------

This is the description for that library
"""
from setuptools import setup


setup(
    name='Bottle-AtlassianConnect',
    version='0.1-dev',
    url='https://bitbucket.org/mrdon/bottle_ac',
    license='APLv2',
    author='Don Brown',
    author_email='mrdon@twdata.org',
    description='Bottle async extension to support Atlassian Connect',
    long_description=__doc__,
    packages=['bottle_ac'],
    # if you would be using a package instead use packages instead
    # of py_modules:
    # packages=['flask_sqlite3'],
    zip_safe=False,
    include_package_data=True,
    platforms='any',
    install_requires=[
        'Bottle-Async',
        'asyncio_redis',
        'aiohttp',
        'asyncio_mongo',
        'PyJWT'
    ],
    classifiers=[
        'Environment :: Web Environment',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: BSD License',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Topic :: Internet :: WWW/HTTP :: Dynamic Content',
        'Topic :: Software Development :: Libraries :: Python Modules'
    ]
)