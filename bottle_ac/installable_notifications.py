import logging
import json
import asyncio
from bottle_ac.util import http_request

from bottle_ac.oauth2 import Oauth2Client
from bottle_ac.installable import add_installable


_log = logging.getLogger(__name__)


@asyncio.coroutine
def send_message(cache, client, msg):
    token = yield from client.get_token(cache, scopes=['send_notification'])

    base_url = client.capabilities_url[0:client.capabilities_url.rfind('/')]
    url = "%s/room/%s/notification?auth_token=%s" % (base_url, client.room_id, token)
    with (yield from http_request('POST', url,
                                      headers={'content-type': 'application/json'},
                                      data=json.dumps({"message": msg}), timeout=10)) as resp:
        if resp.status != 204:
            body = yield from resp.read()
            _log.error("Error sending notification to {url} ({code}, {reason}): {body}".format(code=resp.status,
                                                                                               url=url,
                                                                                      reason=resp.reason, body=body))
        return resp.status


def send_action_message(addon, action):
    # noinspection PyBroadException
    @asyncio.coroutine
    def handle(event):
        try:
            installed_client = event['client']
            name = "Unknown" if not installed_client.group_name else installed_client.group_name
            group_id = "Unknown" if not installed_client.group_id else installed_client.group_id
            msg = "%s (%s) %s" % (name, group_id, action)
            if installed_client.room_id:
                msg += " in a room (%s)" % installed_client.room_id
            else:
                msg += " globally"

            db = addon.mongo_db.default_database['notification_clients']
            clients = yield from db.find()
            for client_data in clients:
                client = Oauth2Client.from_map(client_data)
                try:
                    yield from send_message(addon.redis, client, msg)
                except Exception:
                    logging.exception("Error sending notification")
        except:
            logging.exception("Unable to send notification msg")
    return handle


def add_notifications(app, required_group_id):

    addon = app.addon
    addon.register_event('install', send_action_message(addon, 'installed'))
    addon.register_event('uninstall', send_action_message(addon, 'uninstalled'))

    def validate_group(group_id):
        if int(required_group_id) != group_id:
            _log.error("Attempted to install for group %s when group %s is only allowed" %
                       (group_id, required_group_id))
            return "Only group %s is allowed to install this add-on" % required_group_id

    add_installable(app, "/notifications", allow_global=False, send_events=False,
                    coll_name='notification_clients', validate_group=validate_group)

    # noinspection PyUnusedLocal
    @app.route('/notifications/capabilities')
    def capabilities(request, response):
        plugin_key = app.config.get('PLUGIN_KEY')
        base_url = app.config.get('BASE_URL') + "/notifications"
        if not plugin_key:
            return "Missing PLUGIN_KEY configuration property", 500
        addon_name = app.config.get('ADDON_NAME')
        if not addon_name:
            return "Missing ADDON_NAME configuration property", 500
        from_name = app.config.get('FROM_NAME')
        if not from_name:
            return "Missing FROM_NAME configuration property", 500

        return {
            "links": {
                "self":         base_url + "/capabilities",
                "homepage":     base_url + "/capabilities"
            },
            "key": plugin_key + "-notifications",
            "name": "Notifications for %s" % addon_name,
            "description": "Sends notifications of installs and uninstalls for the %s add-on" % addon_name,
            "capabilities": {
                "installable": {
                    "callbackUrl": base_url + "/installable/"
                },
                "hipchatApiConsumer": {
                    "scopes": [
                        "send_notification"
                    ],
                    "fromName": from_name + " Installs"
                }
            }
        }

__all__ = ['add_notifications']