from .addon import *
from .oauth2 import *
from .installable import *
from .installable_notifications import *
from .webhook import *
from .validate import *

__doc__ = \
    """
    Handles add-on operations, including installations and notifications
    """
