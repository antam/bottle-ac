from functools import wraps
import logging
from urllib.parse import urlparse
import asyncio
from bottle import Bottle, abort, call_maybe_yield
from bottle_ac.installable import add_installable, uninstall_client
from bottle_ac.installable_notifications import add_notifications

from bottle_ac.oauth2 import Oauth2Client, OauthClientInvalidError
import jwt
import os


import asyncio_redis as aredis
import asyncio_mongo as amongo

_log = logging.getLogger(__name__)


@asyncio.coroutine
def create_addon(app=None, init=True):
    a = Addon(app)
    if init:
        a.init_app()

    yield from a.init_db()
    return a


class OAuthInstallationErrorCleanupPlugin:
    ''' This plugin catches ... '''
    name = 'oauth_installation_error'
    api = 2

    def apply(self, callback, route):
        @asyncio.coroutine
        def wrapper(*a, **ka):
            try:
                rv = yield from call_maybe_yield(callback, *a, **ka)
                return rv
            except OauthClientInvalidError as error:
                yield from uninstall_client(route.app.addon, error.client.id)
                a[1].status_code = 204

        return wrapper


def create_addon_app(name, plugin_key=None, addon_name=None, from_name=None, debug=False,
                     base_url="http://localhost:5000", notifications_group_id=None, **kwargs):
    app = Bottle(name)

    app.config.update({
        "DEBUG": True if "true" == os.environ.get("DEBUG", "false") else debug,
        "PLUGIN_KEY": os.environ.get("PLUGIN_KEY", plugin_key),
        "ADDON_NAME": os.environ.get("ADDON_NAME", addon_name),
        "FROM_NAME": os.environ.get("FROM_NAME", from_name),
        "BASE_URL": os.environ.get("BASE_URL", base_url),
        "NOTIFICATIONS_GROUP_ID": os.environ.get("NOTIFICATIONS_GROUP_ID", notifications_group_id)
    })

    app.config.update(kwargs)

    @asyncio.coroutine
    def init():
        yield from create_addon(app)
        add_installable(app, allow_global=False)

        owner_group = app.config.get('NOTIFICATIONS_GROUP_ID')
        if owner_group:
            add_notifications(app, owner_group)

    app.add_hook('before_first_request', init)
    app.install(OAuthInstallationErrorCleanupPlugin())

    return app


class Addon(object):
    def __init__(self, app):
        self.app = app
        self.mongo_db = None
        self.redis = None

    def init_app(self):
        app = self.app

        if app.config.get('DEBUG', False):
            # You must initialize logging, otherwise you'll not see debug output.
            logging.basicConfig()
            logging.getLogger().setLevel(logging.DEBUG)
            aio_log = logging.getLogger("asyncio")
            aio_log.setLevel(logging.INFO)
            aio_log.propagate = True
        else:
            logging.basicConfig()
            aio_log = logging.getLogger("asyncio")
            aio_log.setLevel(logging.WARN)
            logging.getLogger().setLevel(logging.INFO)

        app.events = {}
        self.app.addon = self

    @asyncio.coroutine
    def init_db(self):
        if not hasattr(self.app, 'mongo_db'):
            self.app.mongo_db = yield from self.connect_mongo_db()
        self.mongo_db = self.app.mongo_db

        if not hasattr(self.app, 'redis'):
            self.app.redis = yield from self.connect_redis()
        self.redis = self.app.redis

    @asyncio.coroutine
    def connect_mongo_db(self):
        mongo_url = self.app.config.get('MONGO_URL')
        if not mongo_url:
            mongo_url = "mongo://localhost:27017/test"

        c = yield from amongo.Pool.create(url=mongo_url, poolsize=2)

        return c

    @asyncio.coroutine
    def connect_redis(self):
        redis_url = self.app.config.get('REDIS_URL')
        if not redis_url:
            redis_url = 'redis://localhost:6379'

        url = urlparse(redis_url)

        db = 0
        try:
            if url.path:
                db = int(url.path.replace('/', ''))
        except (AttributeError, ValueError):
            pass

        conn = yield from aredis.Pool.create(url.hostname, port=int(url.port or 6379), db=db,
                                             password=url.password, poolsize=2)
        return conn

    def fire_event(self, name, obj):
        listeners = self.app.events.get(name, [])
        for listener in listeners:
            try:
                yield from listener(obj)
            except:
                logging.exception("Unable to fire event {name} to listener {listener}".format(
                    name=name, listener=listener
                ))

    def register_event(self, name, func):
        _log.debug("Registering event: " + name)
        self.app.events.setdefault(name, []).append(func)

    def unregister_event(self, name, func):
        del self.app.events.setdefault(name, [])[func]

    def event_listener(self, func):
        self.register_event(func.__name__, func)
        return func

    @asyncio.coroutine
    def load_client(self, client_id):
        client_data = yield from self.mongo_db.default_database.clients.find_one(Oauth2Client(client_id).id_query)
        if client_data:
            return Oauth2Client.from_map(client_data)
        else:
            _log.warn("Cannot find client: %s" % client_id)
            abort(400)

    @asyncio.coroutine
    def load_all_clients(self):
        clients = yield from self.mongo_db.default_database.clients.find({})
        return {Oauth2Client.from_map(client_data) for client_data in clients}

    @asyncio.coroutine
    def validate_jwt(self, req):
        jwt_data = req.args.get('signed_request', None)
        if not jwt_data:
            abort(401)

        oauth_id = jwt.decode(jwt_data, verify=False)['iss']
        client = yield from self.load_client(oauth_id)
        data = jwt.decode(jwt_data, client.secret)
        return client, data['prn']

    def require_jwt(self, func):
        @wraps(func)
        def inner(request, response, *args, **kwargs):
            _log.warn("Validating jwt")
            client, user_id = self.validate_jwt(request)
            kwargs.update({
                "client": client,
                "user_id": user_id})
            return func(*args, **kwargs)
        return inner


__all__ = ["create_addon", "create_addon_app"]
