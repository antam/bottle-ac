import json
import asyncio
from bottle_ac.util import http_request

from bottle_ac.oauth2 import Oauth2Client
import logging


_log = logging.getLogger(__name__)


def _invalid_install(resp, msg):
    _log.error("Installation failed: %s" % msg)
    resp.status = "400 %s" % msg


def add_installable(app, base_path="", allow_room=True, allow_global=True, send_events=True,
                    coll_name='clients', validate_group=None):

    # noinspection PyUnusedLocal
    @asyncio.coroutine
    @app.route(base_path + '/installable/', method='POST')
    def on_install(request, response):
        addon = app.addon
        clients = addon.mongo_db.default_database[coll_name]
        data = request.json
        if not data.get('roomId', None) and not allow_global:
            return _invalid_install(response, "This add-on can only be installed in individual rooms.  Please visit the " +
                                    "'Add-ons' link in a room's administration area and install from there.")

        if data.get('roomId', None) and not allow_room:
            return _invalid_install(response, "This add-on cannot be installed in an individual room.  Please visit the " +
                                    "'Add-ons' tab in the 'Group Admin' area and install from there.")

        _log.info("Retrieving capabilities doc at %s" % data['capabilitiesUrl'])

        with (yield from http_request('GET', data['capabilitiesUrl'], timeout=10)) as resp:
            capdoc = yield from resp.read(decode=True)

        if capdoc['links'].get('self', None) != data['capabilitiesUrl']:
            return _invalid_install(response, "The capabilities URL %s doesn't match the resource's self link %s" %
                                    (data['capabilitiesUrl'], capdoc['links'].get('self', None)))

        _log.info("Receiving installation of id {oauthId}".format(oauthId=data['oauthId']))

        client = Oauth2Client(data['oauthId'], data['oauthSecret'], room_id=data['roomId'], capdoc=capdoc)

        try:
            session = yield from client.get_token(addon.redis, token_only=False)
        except Exception as e:
            _log.warn("Error validating installation by receiving token: %s" % e)
            return _invalid_install(response, "Unable to retrieve token using the new OAuth information")

        if validate_group:
            err = validate_group(int(session['group_id']))
            if err:
                return _invalid_install(response, err)

        client.group_id = session['group_id']
        client.group_name = session['group_name']
        yield from clients.remove(client.id_query)
        yield from clients.insert(client.to_map())
        if send_events:
            yield from addon.fire_event('install', {"client": client})

        response.status = 201

    # noinspection PyUnusedLocal
    @asyncio.coroutine
    @app.route(base_path + '/installable/<oauth_id>', method='DELETE')
    def on_uninstall(request, response, oauth_id):
        yield from uninstall_client(app.addon, oauth_id, coll_name, send_events)
        response.status = 204


@asyncio.coroutine
def uninstall_client(addon, oauth_id, db_name='clients', send_events=True):
    client = yield from addon.load_client(oauth_id)
    clients = addon.mongo_db.default_database[db_name]
    client_filter = {"id": oauth_id}
    yield from clients.remove(client_filter)
    if send_events:
        yield from addon.fire_event('uninstall', {"client": client})